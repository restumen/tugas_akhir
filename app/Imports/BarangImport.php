<?php

namespace App\Imports;

use App\Entities\Item;
use Maatwebsite\Excel\Concerns\ToModel;

class BarangImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Item([
            'name' => $row[1],
            'price' => $row[2],
            'stock' => $row[3],
            'image' => $row[4],
        ]);
    }
}
