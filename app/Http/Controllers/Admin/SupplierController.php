<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Suplier;
class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share([
            'data'=>Suplier::all(),
        ]);
        return view('admin.app.supplier.index');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $n = new Suplier();
        $n->nama_toko= $request->nama_toko;
        $n->nama_pemilik = $request->nama_pemilik;
        $n->alamat = $request->alamat;
        $n->nomor_hp = $request->nomor_hp;
        $n->status = $request->status;
        $n->save();
        return redirect()->back();

    }

    /**
     * Show the form for editing the specified resource.
     *'nama_toko','nama_pemilik',
       * 'alamat','nomor_hp','status'
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Suplier $id)
    {
        view()->share([
            'data'=>$id
        ]);
        return view('admin.app.supplier.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Suplier $id)
    {
        $id->update([
            'nama_toko'=>$request->namatoko,
            'nama_pemilik'=>$request->namapemilik,
            'alamat'=>$request->alamat,
            'nomor_hp'=>$request->nomorhp,
            'status'=>$request->status,
        ]);
        return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suplier $id)
    {
        $id->delete();
        return redirect()->back();
    }
}
