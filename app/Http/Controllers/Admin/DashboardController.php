<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\CategoryItem;
use App\Entities\Item;

class DashboardController extends Controller
{
    public function index()
    {
        $category = CategoryItem::all();
        $name = Item::all();
        return view('admin.app.dashboard.index',compact('category','name'));
    }
    public function insertTransaksi(Request $request)
    {
        $request->validate(['image' => 'required']);
       $foto = $request->file('image');
       $nama_file = time()."_".$foto->getClientOriginalName();
       $tujuan_upload = 'data_file';
       $foto->move($tujuan_upload,$nama_file);
        $n = new Item();
        $n->name = $request->name;
        $n->price = $request->price;
        $n->stock = $request->stock;
        $n->image = $nama_file;
        $n->category_id = $request->category_id;
        $n->suplier_id = $request->suplier_id;
        $n->save();
        return redirect()->back();
    }
}
