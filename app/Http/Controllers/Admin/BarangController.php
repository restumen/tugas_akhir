<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Barang;
use App\Entities\Item;
use App\Entities\CategoryItem;
use Alert;
use App\Imports\BarangImport;
use Maatwebsite\Excel\Facades\Excel;

class BarangController extends Controller
{
    public function tampilan(){
        $tampilan = Barang::all();
        return view('atribut',compact('tampilan'));
    
       }
    public function insert(Request $request)
   {
       $request->validate(['image' => 'required']);
       $foto = $request->file('image');
       $nama_file = time()."_".$foto->getClientOriginalName();
       $tujuan_upload = 'data_file';
       $foto->move($tujuan_upload,$nama_file);
       $data->kategori_id = $request->kategori_id;
       $data->image = $nama_file;
    //    $data->deskripsi = $request->deskripsi;
       $data->save();
       Alert::success('Data berhasil tersimpan', 'success');
       return redirect()->back();
    }
    public function tampil($id){
      $itemsDasi = Item::where('category_id', $id)->get();
      $dropdown = CategoryItem::all();
      $tampil_nama = CategoryItem::select("name")->where("id",$id)->value("name");
      return view('website/dasi',compact('itemsDasi','dropdown','tampil_nama'));
    }
    // public function tampil_sepatu(){
    //   $itemsSepatu = Item::select()->where('category_id', '=', 4)->get();
    //   return view('website/sepatu',compact('itemsSepatu'));
    // }
    // public function tampil_kaoskaki(){
    //   $itemsKaos = Item::select()->where('category_id', '=', 5)->get();
    //   return view('website/kaos_kaki',compact('itemsKaos'));
    // }
    // public function tampil_topi(){
    //   $itemsTopi = Item::select()->where('category_id', '=', 6)->get();
    //   return view('website/topi',compact('itemsTopi'));
    // }
    // public function tampil_atributperamuka(){
    //   $itemsAtributpramuka = Item::select()->where('category_id', '=', 7)->get();
    //   return view('website/atribut_pramuka',compact('itemsAtributpramuka'));
    // }
    // public function tampil_badge(){
    //   $itemsBadge = Item::select()->where('category_id', '=', 8)->get();
    //   return view('website/badge_jurusan',compact('itemsBadge'));
    // }
    public function import_excel(Request $request) 
  {
    // validasi
    $this->validate($request, [
      'file' => 'required|mimes:csv,xls,xlsx'
    ]);
 
    // menangkap file excel
    $file = $request->file('file');
 
    // membuat nama file unik
    $nama_file = rand().$file->getClientOriginalName();
 
    // upload ke folder file_siswa di dalam folder public
    $file->move('file_barang',$nama_file);
 
    // import data
    Excel::import(new BarangImport, public_path('/file_barang/'.$nama_file));
 
    // notifikasi dengan session
    Session::flash('sukses','Data Siswa Berhasil Diimport!');
 
    // alihkan halaman kembali
    return redirect('/item');
  }

    public function tampil_sepatu(){
      $itemsSepatu = Item::select()->where('category_id', '=', 4)->get();
      return view('website/sepatu',compact('itemsSepatu'));
    }
    public function tampil_kaoskaki(){
      $itemsKaos = Item::select()->where('category_id', '=', 5)->get();
      return view('website/kaos_kaki',compact('itemsKaos'));
    }
    public function tampil_topi(){
      $itemsTopi = Item::select()->where('category_id', '=', 6)->get();
      return view('website/topi',compact('itemsTopi'));
    }
    public function tampil_atributperamuka(){
      $itemsAtributpramuka = Item::select()->where('category_id', '=', 7)->get();
      return view('website/atribut_pramuka',compact('itemsAtributpramuka'));
    }
    public function tampil_badge(){
      $itemsBadge = Item::select()->where('category_id', '=', 8)->get();
      return view('website/badge_pramuka',compact('itemsBadge'));
    }
}
