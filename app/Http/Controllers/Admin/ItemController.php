<?php

namespace App\Http\Controllers\Admin;

use App\Entities\CategoryItem;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Entities\Suplier;
use Alert;
use App\Imports\BarangImport;
use Maatwebsite\Excel\Facades\Excel;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share(
            [
                'data'=> Item::all(),
                'category'=>CategoryItem::all(),
                'suplier'=>Suplier::all()
            ]
        );
        return view('admin.app.item.index');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['image' => 'required']);
       $foto = $request->file('image');
       $nama_file = time()."_".$foto->getClientOriginalName();
       $tujuan_upload = 'data_file';
       $foto->move($tujuan_upload,$nama_file);
        $n = new Item();
        $n->name = $request->name;
        $n->price = $request->price;
        $n->stock = $request->stock;
        $n->image = $nama_file;
        $n->category_id = $request->category_id;
        $n->suplier_id = $request->suplier_id;
        $n->save();
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $id)
    {
        view()->share([
            'data'=>$id
        ]);
        return view('admin.app.item.edit');
    }

    /**
     * Update the specified resource in storage.
     *'name','price','stock','image'
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Item $id)
    {
        if($request->hasFile('image')){
            $request->validate(['image' => 'required']);
            $foto = $request->file('image');
            $nama_file = time()."_".$foto->getClientOriginalName();
            $tujuan_upload = 'data_file';
            $foto->move($tujuan_upload,$nama_file);
            $id->update([
                'name'=>$request->name,
                'price'=>$request->price,
                'stock'=>$request->stock,
                'image'=>$nama_file,
            ]);
            return redirect()->route('item.index');
        }
        else{
            $id->update([
                'name'=>$request->name,
                'price'=>$request->price,
                'stock'=>$request->stock,
            ]);
            return redirect()->route('item.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Item $id)
    {
        $id->delete();
        return redirect()->back();
    }

    public function import_excel(Request $request) 
  {
    // validasi
    $this->validate($request, [
      'file' => 'required|mimes:csv,xls,xlsx'
    ]);
 
    // menangkap file excel
    $file = $request->file('file');
 
    // membuat nama file unik
    $nama_file = rand().$file->getClientOriginalName();
 
    // upload ke folder file_siswa di dalam folder public
    $file->move('file_barang',$nama_file);
 
    // import data
    Excel::import(new BarangImport, public_path('/file_barang/'.$nama_file));
 
    // notifikasi dengan session
    Session::flash('sukses','Data Siswa Berhasil Diimport!');
 
    // alihkan halaman kembali
    return redirect('/item');
  }
}
