<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $rulesUsername = [
            'username' => $request->input('nm'),
            'password' => $request->input('pwd'),
        ];
        $rulesEmail = [
            'email' => $request->input('nm'),
            'password' => $request->input('pwd'),
        ];
        if (Auth::attempt($rulesUsername)){
            return redirect()->route('dash.index');
        }else if(Auth::attempt($rulesEmail)){
            return redirect()->route('dash.index');
        }else{
            return redirect()->back()->with(['error'=>'S']);
        }
    }
    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
