<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\User;
use Illuminate\Contracts\Encryption\DecryptException;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share([
            'data'=>User::all(),
        ]);
        return view('admin.app.admin.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->username = $request->username;
        $data->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $id)
    {

        view()->share([
            'data'=>$id,
        ]);
        return view('admin.app.admin.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $id)
    {
        if($request->has('new_pass')){
            $pass = bcrypt($request->new_pass);
            $id->update([
                'username'=> $request->username,
                'name'=> $request->name,
                'email'=> $request->email,
                'password'=> $pass,
            ]);
        }else{
            $id->update([
                'username'=> $request->username,
                'name'=> $request->name,
                'email'=> $request->email,
                'password'=> $request->password,
            ]);
        }
        return redirect()->route('admin.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(User $id)
    {
        $id->delete();
        return redirect()->back();
    }
}
