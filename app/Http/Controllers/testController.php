<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\CategoryItem;

class testController extends Controller
{
	public function home()
	{
    	$dropdown = CategoryItem::all();
    	return view('index',compact('dropdown'));
	}
	public function about()
	{
		$about = CategoryItem::all();
		return view('about',compact('about'));
	}
}
