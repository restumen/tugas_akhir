<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\CategoryItem;

class create_kategori extends Controller
{
    //
    public function index()
    {
        view()->share(
            [
                'data'=> CategoryItem::all(),
                'category'=>CategoryItem::all()
            ]
        );
        return view('kategori');
    }
    public function store(Request $request)
    {   

        $n = new CategoryItem();
        $n->name = $request->name;
        $n->save();
        return redirect()->back();

    }
}
