<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\CategoryItem;
class Item extends Model
{
    protected $table='item';
    protected $fillable=['name','price','stock','image'];

    public function category()
    {
        return $this->belongsTo(CategoryItem::class);
    }
}
