<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Item;
class CategoryItem extends Model
{
    protected $table='category_item';
    protected $fillable=['name'];

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
