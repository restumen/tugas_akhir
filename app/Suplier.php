<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suplier extends Model
{
    //
    protected $table = 'suplier';
    protected $primaryKey = 'id';
    protected $fillable = ['nama_toko','nama_pemilik',
    	'alamat','nomor_hp','status',];
}
