<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id';
    protected $fillable = ['name','price',
    	'stok','suplier_id','image',];

    public function kategori()
	{
		return $this->belongsTo(Kategori::class);
	}

	public function suplier()
	{
		return $this->belongsTo(Suplier::class);
	}

	public $timestamps = false;
}
