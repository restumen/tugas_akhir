@extends('admin.layouts.app')
@section('content-header')
<link rel="shortcut icon" href="image/favicon.ico">
    Data Siswa <br> <br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-student">
        <i class="fa fa-plus"></i> Tambah Transaksi
    </button>
@endsection

@section('header-small')

@endsection
@section('content')
<div class="modal fade" id="create-student">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <form action="{{route('item.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <select name="name" class="form-control" id="listdata" onchange="listdata();">
                        <option disabled selected value="">Pilih Nama</option>
                        @foreach($name as $list)
                        <option value="{{$list->id}}" name="{{$list->name}}" category="{{$list->category}}" price="{{$list->price}}" stock="{{$list->stock}}">{{$list->name}}</option>
                        @endforeach
                        </select>
                        <!-- <label for="">Nama :</label>
                        <input type="text" class="form-control" name="name"> -->
                        <label for="">Kategory :</label>
                        <input type="text" class="form-control" name="name" id="name">
                        <label for="">Harga :</label>
                        <input type="text" name="price" class="form-control" id="price">
                        <label for="">Stok :</label>
                        <input type="text" name="stock" class="form-control" id="stock">
                        <label for="">Kembalian :</label>
                        <input type="text" name="stock" class="form-control">
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Rekap Transaksi</h3>
        </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    
@endsection
<script>
    function (){
        var listdata = document.getElementById('listdata');
        document.getElementById("price").value = listdata.option[list.selectedIndex].getAttribute('price');
        document.getElementById("category").value = listdata.option[list.selectedIndex].getAttribute('category');
        document.getElementById("stock").value = listdata.option[list.selectedIndex].getAttribute('stock');
    }
    </script>
