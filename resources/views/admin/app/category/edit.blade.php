@extends('admin.layouts.app')
@section('content-header')
@endsection
@section('header-small')
@endsection
@section('content')
<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Form</h3>
                </div>
                <form action="{{route('category.update',$data->id)}}" method="POST">
                    @method('put')
                    @csrf
                  <div class="box-body">
                    <div class="form-group">
                      <label for="">Nama Kategori Barang</label>
                      <input type="text" name="name" class="form-control" id="" placeholder="Masukkan Nama Kategori Barang" value="{{$data->name}}">
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
</div>
@endsection
