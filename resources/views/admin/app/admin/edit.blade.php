@extends('admin.layouts.app')
@section('content-header')
@endsection
@section('header-small')
@endsection
@section('content')
<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Form</h3>
                </div>
                <form action="{{route('admin.update',$data->id)}}" method="POST">
                    @method('put')
                    @csrf
                  <div class="box-body">
                    <div class="form-group">
                      <label for="">Nama Admin</label>
                      <input type="text" name="name" class="form-control" id="" placeholder="Masukkan Nama ADmin" value="{{$data->name}}">
                    </div>
                    <div class="form-group">
                        <label for="">Username Admin</label>
                        <input type="text" name="username" class="form-control" id="" placeholder="Masukkan Username Admin" value="{{$data->username}}">
                      </div>
                      <div class="form-group">
                        <label for="">Email Admin</label>
                        <input type="text" name="email" class="form-control" id="" placeholder="Masukkan Email Admin" value="{{$data->email}}">
                      </div>
                      <div class="form-group">
                        <label for="">Password Admin</label>
                        <input type="text" name="new_pass" class="form-control" id="" placeholder="Masukkan Password baru apabila ingin merubah">
                        <input type="hidden" name="password" class="form-control" id="" placeholder="Masukkan Password baru apabila ingin merubah" value="{{$data->password}}">
                      </div>
                  </div>


                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
</div>
@endsection
