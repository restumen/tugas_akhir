@extends('admin.layouts.app')
@section('content-header')

    Data Admin <br> <br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-student">
        <i class="fa fa-plus"></i> Tambah admin
    </button>
@endsection
@section('header-small')

@endsection
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data admin</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $read)
                    <tr>
                        <td>{{$read->name}}</td>
                        <td>
                            <form action="{{ route('admin.delete', $read->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-warning" href="{{ route('admin.edit', $read->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    {{-- Tambah Data --}}
    {{-- Modal --}}

    <div class="modal fade" id="create-student">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah admin</h4>
                </div>
                <form action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="modal-body">
                        <label for="">Nama :</label>
                        <input type="text" class="form-control" name="name">

                        <label for="">email :</label>
                        <input type="email" class="form-control" name="email">

                        <label for="">username :</label>
                        <input type="text" class="form-control" name="username">

                        <label for="">password :</label>
                        <input type="password" class="form-control" name="password">
                        <hr>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Tambah Data --}}
@endsection
@push('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    @endpush
