@extends('admin.layouts.app')
@section('content-header')
@endsection
@section('header-small')
@endsection
@section('content')
<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Form</h3>
                </div>
                <form action="{{route('item.update',$data->id)}}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                  <div class="box-body">
                    <div class="form-group">
                      <label for="">Nama Barang</label>
                      <input type="text" name="name" class="form-control"  value="{{$data->name}}">
                    </div>
                    <div class="form-group">
                            <label for="">Harga Barang</label>
                            <input type="text" name="price" class="form-control"  value="{{$data->price}}">
                    </div>
                    <div class="form-group">
                        <label for="">Stok Barang</label>
                         <input type="text" name="stock" class="form-control"  value="{{$data->stock}}">
                    </div>
                    <div class="form-group">
                        <label for="">Gambar Barang</label>
                        <img src="{{asset('data_file/'.$data->image)}}" alt="" style="width: 50px;height: 50px">
                        <input type="file" name="image">
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
</div>
@endsection
