@extends('admin.layouts.app')
@section('content-header')
<link rel="shortcut icon" href="image/favicon.ico">
    Data Siswa <br> <br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-student">
        <i class="fa fa-plus"></i> Tambah Barang Jualan
    </button>
    <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel">
            IMPORT EXCEL
        </button>
@endsection
@section('header-small')

@endsection
@section('content')

{{-- notifikasi form validasi --}}
        @if ($errors->has('file'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('file') }}</strong>
        </span>
        @endif
 
        {{-- notifikasi sukses --}}
        @if ($sukses = Session::get('sukses'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $sukses }}</strong>
        </div>
        @endif
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Barang</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $read)
                    <tr>
                        <td>{{$read->name}}</td>
                        <td>{{$read->category['name']}}</td>
                        <td>{{$read->price}}</td>
                        <td>{{$read->stock}}</td>
                        <td>
                            <form action="{{ route('item.delete', $read->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-warning" href="{{ route('item.edit', $read->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    {{-- Tambah Data --}}
    {{-- Modal --}}

    <div class="modal fade" id="create-student">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <form action="{{route('item.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <label for="">Nama :</label>
                        <input type="text" class="form-control" name="name">
                        <label for="">Harga :</label>
                        <input type="text" name="price" class="form-control">
                        <label for="">Stok :</label>
                        <input type="text" name="stock" class="form-control">
                        
                        <label for="">Category</label>
                        <!-- <input type="text" name="category_id" class="form-control"> -->
                        <select class="form-control" name="category_id">
                            <option></option>
                            @foreach($category as $data)
                            <option value="{{$data->id}}">{{$data->name}}</option>
                            @endforeach
                        </select>
                        <label for="">Suplier</label>
                        <select class="form-control" name="suplier_id">
                            <option></option>
                            @foreach($suplier as $data)
                            <option value="{{$data->id}}">{{$data->nama_toko}}</option>
                            @endforeach
                        </select>
                        <hr>
                        <label for="">Gambar</label>
                        <input type="file" name="image" class="form-control">



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    
 
        <!-- Import Excel -->
        <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="item/import_excel" enctype="multipart/form-data">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                        </div>
                        <div class="modal-body">
 
                            {{ csrf_field() }}
                            
 
                            <label>Pilih file excel</label>
                            <div class="form-group">
                                <input type="file" name="file" required="required">
                            </div>
 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    {{-- Tambah Data --}}
@endsection
@push('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    @endpush
