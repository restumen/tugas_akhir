@extends('admin.layouts.app')
@section('content-header')
@endsection
@section('header-small')
@endsection
@section('content')
<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Form</h3>
                </div>
                <form action="{{route('supplier.update',$data->id)}}" method="POST">
                    @method('put')
                    @csrf
                  <div class="box-body">
                    <div class="form-group">
                            <label for="">Nama Toko :</label>
                            <input type="text" class="form-control" name="namatoko" value="{{$data->nama_toko}}">
                    </div>
                    <div class="form-group">
                            <label for="">Nama Pemilik :</label>
                            <input type="text" class="form-control" name="namapemilik" value="{{$data->nama_pemilik}}">
                    </div>
                    <div class="form-group">
                            <label for="">Alamat :</label>
                            <input type="text" class="form-control" name="alamat" value="{{$data->alamat}}">
                    </div>
                    <div class="form-group">
                            <label for="">Nomor Hp :</label>
                            <input type="text" class="form-control" name="nomorhp" value="{{$data->nomor_hp}}">
                    </div>
                    <div class="form-group">
                            <label for="">Status :</label>
                            <input type="text" class="form-control" name="status" value="{{$data->status}}">
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
</div>
@endsection
