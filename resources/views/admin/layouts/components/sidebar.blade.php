<aside class="main-sidebar">
    {{-- <!-- sidebar: style can be found in sidebar.less --> --}}
    <section class="sidebar">
        {{-- <!-- Sidebar user panel --> --}}
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('dist/img/avatar04.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <i class="fa fa-circle text-success"></i> Online
                </a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Inputan Barang</li>
            <li class=""><a href="{{route('dash.index')}}"><i class="fa fa-book"></i> <span>Transaksi</span></a></li>


            <li class="treeview">
                <a href="#"><i class="fa fa-book"></i>
                    <span>Barang</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('item.index') }}"><i class="fa fa-circle-o"></i> List Barang</a></li>
                    <li><a href="{{route('category.index')}}"><i class="fa fa-circle-o"></i>Kategori Barang</a></li>
                </ul>
            </li>

            <li class=""><a href="{{route('supplier.index')}}"><i class="fa fa-book"></i> <span>Suplier</span></a></li>
            <li class=""><a href="{{route('admin.index')}}"><i class="fa fa-book"></i> <span>Admin</span></a></li>
            <li class=""><a href=""><i class="fa fa-key"></i> <span>Status</span></a></li>








        </ul>
    </section>
    {{-- <!-- /.sidebar --> --}}
</aside>
