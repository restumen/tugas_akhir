@extends('admin.layouts.skeleton')
@section('app')
    <div class="wrapper">
    @include('admin.layouts.components.navbar')
    {{-- <!-- Left side column. contains the logo and sidebar --> --}}
    @include('admin.layouts.components.sidebar')
    {{-- <!-- Content Wrapper. Contains page content --> --}}
        {{-- <!-- /.content-wrapper --> --}}
        <div class="content-wrapper">
    {{-- <!-- Content Header (Page header) --> --}}
    <section class="content-header">
        <h1>
            @yield('content-header')
            <small>@yield('header-small')</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            @yield('content')
        </div>
        <!-- /.row (main row) -->
    </section><!-- /.content -->
    <script id="cid0020000235501600643" data-cfasync="false" async src="//st.chatango.com/js/gz/emb.js" style="width: 400px;height: 600px;">{"handle":"smeaskoperasi","arch":"js","styles":{"a":"000000","b":100,"c":"FFFFFF","d":"FFFFFF","k":"000000","l":"000000","m":"000000","n":"FFFFFF","p":"10","q":"000000","r":100,"pos":"br","cv":1,"cvbg":"CC0000","cvw":200,"cvh":30,"ticker":1,"fwtickm":1}}</script>
    </div>

        @include('admin.layouts.components.footer')
    </div>

    @endsection
