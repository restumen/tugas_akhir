@extends('admin.layouts.skeleton')
@section('app')
<body style="background-color: #fffa78"></body>
    <div class="login-box">
        <div class="login-logo">
            <a style="color:#ad6836;"><b>SMEAS</b>.Koperasi</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <h3><p class="login-box-msg">-----  Selamat Datang  -----</p></h3>

            <form action="{{route('login')}}" method="post">
                @csrf
                <div class="form-group has-feedback">
                    <input name="nm" type="text" class="form-control" placeholder="Email/Username">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input name="pwd" type="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn btn-block btn-flat" 
                        style="background-color:#ad6836;"><a style="color:white;">Masuk</a></button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <div class="social-auth-links text-center">

            </div>
            <!-- /.social-auth-links -->

{{--            <a href="#">I forgot my password</a><br>--}}
{{--            <a href="register.html" class="text-center">Register a new membership</a>--}}

        </div>
        <!-- /.login-box-body -->
    </div>
    @endsection
