@extends('template')

@section('content')
<header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light navbar-default bootsnav" style="background-color: #fffa78">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand"><img src="image/smeas11.png" class="logo" alt="" width="190"></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item"><a class="nav-link" href="../">Beranda</a></li>

                        <li class="dropdown megamenu-fw">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Kategori</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Atribut</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    @foreach($dropdown as $data)
                                                    <li><a href="{{route('kategori.show',$data->id)}}">{{$data->name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Alat Tulis</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="#">Pensil</a></li>
                                                    <li><a href="#">Penghapus</a></li>
                                                    <li><a href="#">Bolpoin</a></li>
                                                    <li><a href="#">Penggaris</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Makanan Ringan</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="#">Bidaran</a></li>
                                                    <li><a href="#">Makaroni</a></li>
                                                    <li><a href="#">Krupuk</a></li>
                                                    <li><a href="#"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Minuman</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="#">-</a></li>
                                                    <li><a href="#">-</a></li>
                                                    <li><a href="#">-</a></li>
                                                    <li><a href="#">-</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                    </div>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="../about">Tentang Kami</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="../transaksi">Transaksi</a></li> -->
                       
                        
                        <!-- <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">SHOP</a>
                            <ul class="dropdown-menu">
                                <li><a href="cart.html">Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="my-account.html">My Account</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="shop-detail.html">Shop Detail</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Our Service</a></li> -->
                        <!-- <li class="nav-item"><a class="nav-link" href="kontak">Contact Us</a></li> -->
                    </ul>
                </div>
    </header>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('template/images/smeascopy.jpg')}}" alt="" width="1366" height="1000">
        <div class="container">
          <div class="carousel-caption text-left">
            <!-- <h1>Example headline.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('template/images/smkbisa.jpg')}}" alt="" width="1366" height="1000">
        <div class="container">
          <div class="carousel-caption">
           <!--  <h1>Another example headline.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('template/images/smeascopy.jpg')}}" alt="" width="1366" height="1000">
        <div class="container">
          <div class="carousel-caption text-right">
            <!-- <h1>One more for good measure.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p> -->
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

<section class="ftco-section services-section p-0" style="background-color: #fffa78;" >
    <div class="container">
        <marquee behavior="scroll" class="font-weight-bold  pt-2" direction="left" >
        <a style="color: #a85f1e">SMEAS Koperasi – Menyediakan segala keperluan sekolah</a></marquee>
    </div>
</section>
    
@stop

