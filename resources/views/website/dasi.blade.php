@extends('template')

@section('content')
<header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light navbar-default bootsnav" style="background-color: #fffa78">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand"><img src="{{asset('template/images/smeas11.png')}}" class="logo" alt="" width="190"></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item"><a class="nav-link" href="../">Beranda</a></li>

                        <li class="dropdown megamenu-fw">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Kategori</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Atribut</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    @foreach($dropdown as $data)
                                                    <li><a href="{{route('kategori.show',$data->id)}}">{{$data->name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Alat Tulis</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="#">Pensil</a></li>
                                                    <li><a href="#">Penghapus</a></li>
                                                    <li><a href="#">Bolpoin</a></li>
                                                    <li><a href="#">Penggaris</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Makanan Ringan</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="shop.html">Bidaran</a></li>
                                                    <li><a href="shop.html">Makaroni</a></li>
                                                    <li><a href="shop.html">Krupuk</a></li>
                                                    <li><a href="shop.html"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Accessories</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="shop.html">Bags</a></li>
                                                    <li><a href="shop.html">Sunglasses</a></li>
                                                    <li><a href="shop.html">Fragrances</a></li>
                                                    <li><a href="shop.html">Wallets</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                    </div>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="../about">Tentang Kami</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="../website/status">Status Koperasi</a></li> -->
                       
                        
                        <!-- <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">SHOP</a>
                            <ul class="dropdown-menu">
                                <li><a href="cart.html">Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="my-account.html">My Account</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="shop-detail.html">Shop Detail</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Our Service</a></li> -->
                        <!-- <li class="nav-item"><a class="nav-link" href="kontak">Contact Us</a></li> -->
                    </ul>
                </div>
    </header>
    <h1 style="margin-left: 20px; padding-top:10px;"><u>{{$tampil_nama}}</u></h1>
	
<div class="album py-5">
    <div class="container">

      <div class="row">
      @foreach($itemsDasi as $it)     
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
          <center><img src="{{ asset('/data_file/'.$it->image) }}" class="img-responsive" alt="Project Title" 
          class="card-img-top" alt="..." height="200" width="200" style="margin-top: 10px; " ></center>
            <div class="card-body">
              <center><h3><b><p class="card-text">{{$it->name}}</p></b></h3></center>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                <div class="container">
                <p><a>STOK :  {{$it->stock}} </a></p>
                <a>HARGA : </a><?php echo "Rp.".number_format($it->price, 0, ".", ".");?>
                @if($it->stock == 0)
                <p>
                <a style><b style="color:red">Stok Habis</b></a>
                @else
                <p>
                <a style><b style="color:black">Stok Tersedia{{$it->deskripsi}}</b></a>
                @endif
            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
        
@endsection