@extends('template')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>Tambah Barang</title>
</head>
<body>
<form class="form-group" action="/post" method="post" enctype="multipart/form-data">
  @csrf
  <div class="container">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Nama Barang</label>
      <input type="text" name="nama" class="form-control">
    </div>
    <div class="form-group col-md-6">
      <label for="inputEmail5">Stok Barang</label>
      <input type="text" name="jumlah" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Harga</label>
    <input type="text" name="harga" class="form-control" >
  </div>
  <div class="form-group">
    <label for="inputAddress2">Kategori Barang</label>
   <select name="kategori_id" class="form-control" id="list" onchange="myFunction()">
      @foreach($tampilan as $datanyobak)
      <option value="" hidden>~ PILIH KATEGORI ~</option>
          <option value="{{$datanyobak->id}}">{{$datanyobak->nama_kategori}}</option>
          @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="inputAddress2">Suplier ID</label>
    <input type="text" name="suplier_id" class="form-control" >
  </div>
  <div class="form-group">
    <input type="file" name="foto" class="file" id="validatedCustomFile">
    </div>
  
  </div>
</div>
    <button type="submit" class="btn btn-primary">Save</button><br>
  
</form>
</body>
</html>

<script>
function myFunction(){
      var list = document.getElementById("list");
      document.getElementById("kategori").value = list.options[list.selectedIndex].getAttribute('id_kategori');
      // document.getElementById("qty").value = list.options[list.selectedIndex].getAttribut('data-qty');
    }
</script>

@endsection