@extends('admin.layouts.app')
@section('content-header')

    Data Siswa <br> <br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-student">
        <i class="fa fa-plus"></i> Tambah Kategori
    </button>
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#create-student">
            <i class="fa fa-plus"></i> Import Data Barang
        </button>
@endsection
@section('header-small')

@endsection
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data Barang</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $read)
                    <tr>
                        <td>{{$read->name}}</td>
                        <td>
                            <form action="{{ route('item.delete', $read->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-info" href="{{ route('item.show', $read->id) }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a class="btn btn-sm btn-warning" href="{{ route('item.edit', $read->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-sm btn-danger" onclick="return confirm('Yakin menghapus data ?')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

    {{-- Tambah Data --}}
    {{-- Modal --}}

    <div class="modal fade" id="create-student">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <form action="simpan_kategori" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="modal-body">
                        <label for="">Nama :</label>
                        <input type="text" class="form-control" name="name">
                        <hr>
                        

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- Tambah Data --}}
@endsection
@push('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    @endpush
