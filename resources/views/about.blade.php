@extends('template')

@section('content')
<header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light navbar-default bootsnav" style="background-color: #fffa78">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.html"><img src="image/smeas11.png" class="logo" alt="" width="190"></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item"><a class="nav-link" href="../">Beranda</a></li>

                        <li class="dropdown megamenu-fw">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Kategori</a>
                            <ul class="dropdown-menu megamenu-content" role="menu">
                                <li>
                                    <div class="row">
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Atribut</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href=Bat Jurusan></a></li>
                                                    <li><a href="../website/badge_jurusan">Badge Jurusan</a></li>
                                                    <li><a href="../website/dasi">Dasi</a></li>
                                                    <li><a href="../website/kaos_kaki">Kaos Kaki</a></li>
                                                    <li><a href="../website/topi">Topi</a></li>
                                                    <li><a href="../website/atribut_pramuka">Atribut Pramuka</a></li>
                                                    <li><a href="../website/sepatu">Sepatu</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Alat Tulis</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="#">Pensil</a></li>
                                                    <li><a href="#">Penghapus</a></li>
                                                    <li><a href="#">Bolpoin</a></li>
                                                    <li><a href="#">Penggaris</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Makanan Ringan</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="shop.html">Bidaran</a></li>
                                                    <li><a href="shop.html">Makaroni</a></li>
                                                    <li><a href="shop.html">Krupuk</a></li>
                                                    <li><a href="shop.html"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-menu col-md-3">
                                            <h6 class="title">Accessories</h6>
                                            <div class="content">
                                                <ul class="menu-col">
                                                    <li><a href="shop.html">Bags</a></li>
                                                    <li><a href="shop.html">Sunglasses</a></li>
                                                    <li><a href="shop.html">Fragrances</a></li>
                                                    <li><a href="shop.html">Wallets</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                    </div>
                                    <!-- end row -->
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="../about">Tentang Kami</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="../website/status">Status Koperasi</a></li> -->
                       
                        
                        <!-- <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">SHOP</a>
                            <ul class="dropdown-menu">
                                <li><a href="cart.html">Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                                <li><a href="my-account.html">My Account</a></li>
                                <li><a href="wishlist.html">Wishlist</a></li>
                                <li><a href="shop-detail.html">Shop Detail</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="service.html">Our Service</a></li> -->
                        <!-- <li class="nav-item"><a class="nav-link" href="kontak">Contact Us</a></li> -->
                    </ul>
                </div>
    </header>
<div class="all-title-box" style="background-image: url('template/images/smeascopy.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>ABOUT US</h2>
                    <!-- <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">ABOUT US</li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start About Page  -->
    <div class="about-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="noo-sh-title">SMEAS.Koperasi <span>By SMKN 1 Surabaya</span></h2>
                    <p>"Bingung mau kemana, Ya SMEAS Koperasi aja...."</p>
                    <p>SMEAS Koperasi adalah tefa yang dibuka oleh jurusan akutansi yang berguna untuk menyediakan barang-barang yang dibutuhkan oleh siswa.
                        </p>
                </div>
                <div class="col-lg-6">
                    <div class="banner-frame"> <img class="img-thumbnail img-fluid" src="template/images/smeascopy.jpg" alt="" />
                    </div>
                </div>
            </div>
            <!-- <div class="row my-5">
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Trusted</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Professional</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="service-block-inner">
                        <h3>We are Expert</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

@endsection