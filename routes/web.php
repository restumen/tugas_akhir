
<?php

// Route::get('/', function () {
//     return view('index');
// });
route::get('/','testController@home');
route::get('about','testController@about');

Route::group(['namespace'=>'Admin'],function (){
    Route::get('auth/login','AuthController@showLoginForm')->name('auth.showloginform');
    Route::post('/','AuthController@login')->name('login');
    Route::get('logout','AuthController@logout')->name('logout');
});


Route::group([
    'namespace'     => 'Admin',
    'middleware'    => 'is_admin',
    'prefix'        => 'dashboard'
],function (){
    Route::name('dash.')->group(function (){
        Route::get('','DashboardController@index')->name('index');
    });
});

Route::group([
    'namespace'     => 'Admin',
    'middleware'    => 'is_admin',
'prefix'        => 'item'
],function (){
    Route::name('item.')->group(function (){
        Route::get('','ItemController@index')->name('index');
        Route::get('{id}','ItemController@show')->name('show');
        Route::post('','ItemController@store')->name('store');
        Route::get('edit/{id}','ItemController@edit')->name('edit');
        Route::put('{id}','ItemController@update')->name('update');
        Route::delete('{id}','ItemController@delete')->name('delete');
        Route::post('item/import_excel', 'ItemController@import_excel');
    });
});


Route::group([
    'namespace'     => 'Admin',
    'middleware'    => 'auth',
    'prefix'        => 'supplier'
],function (){
    Route::name('supplier.')->group(function (){
        Route::get('','SupplierController@index')->name('index');
        Route::post('','SupplierController@store')->name('store');
        Route::get('edit/{id}','SupplierController@edit')->name('edit');
        Route::put('{id}','SupplierController@update')->name('update');
        Route::delete('{id}','SupplierController@destroy')->name('delete');
    });
});


Route::group([
    'namespace'     => 'Admin',
    'middleware'    => 'auth',
    'prefix'        => 'category'
],function (){
    Route::name('category.')->group(function (){
        Route::get('','CategoryItemController@index')->name('index');
        Route::post('','CategoryItemController@store')->name('store');
        Route::get('edit/{id}','CategoryItemController@edit')->name('edit');
        Route::put('{id}','CategoryItemController@update')->name('update');
        Route::delete('category.delete/{id}','CategoryItemController@delete')->name('delete');
    });
});

Route::group([
    'namespace'     => 'Admin',
    'middleware'    => 'auth',
    'prefix'        => 'admin'
],function (){
    Route::name('admin.')->group(function (){
        Route::get('','AdminController@index')->name('index');
        Route::post('','AdminController@store')->name('store');
        Route::get('edit/{id}','AdminController@edit')->name('edit');
        Route::put('{id}','AdminController@update')->name('update');
        Route::delete('category.delete/{id}','AdminController@delete')->name('delete');
    });
});


Route::get('beranda', function () {
    return view('index');
});
Route::get('transaksi', function () {
    return view('transaksi');
});


Route::get('website/{id}', 'Admin\BarangController@tampil')->name('kategori.show');
// Route::get('website/dasi', 'Admin\BarangController@tampil');
//
Route::get('website/kaos_kaki', 'Admin\BarangController@tampil_kaoskaki');

Route::get('website/topi', 'Admin\BarangController@tampil_topi');

Route::get('website/atribut_pramuka', 'Admin\BarangController@tampil_atributperamuka');

Route::get('website/sepatu', 'Admin\BarangController@tampil_sepatu');

Route::get('website/badge_jurusan', 'Admin\BarangController@tampil_badge');

Route::get('website/status', function () {
    return view('website/status');
});

// Route::get('/dashboard', 'DashboardController@index');
