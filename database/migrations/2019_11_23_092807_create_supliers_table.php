<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_toko');
            $table->string('nama_pemilik');
            $table->string('alamat');
            $table->string('nomor_hp');
            $table->string('status');
            $table->timestamps();
        });

        Schema::table('item', function (Blueprint $table) {
            $table->foreign('suplier_id')->references('id')->on('suplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suplier');
    }
}
