<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('password');
        factory(User::class)->create(
            [
                'email'=>'admin@smeas.com',
                'username'=>'admin',
                'role'=>'guru',
                'password'=>$password,
            ]
            );
    }
}
